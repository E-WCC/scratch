## Scratch workspace

This is a **scratch** repository.  Don't put anything important here.
The idea is that you can commit/push/pull/branch/merge in other words, do whatever 
you want without hurting anything.
